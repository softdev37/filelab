/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filelab;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class WriteFriendList {

    public static void main(String[] args) {
        ArrayList<Friend> friendList = new ArrayList<>();
        friendList.add(new Friend("Karntima ", 21, "0884561237"));
        friendList.add(new Friend("tima ", 23, "0944561237"));
        friendList.add(new Friend("ma ", 25, "0948745637"));

        FileOutputStream fos = null;
        try {
            //friends.dat
            File file = new File("List_of_friend.dat");

            fos = new FileOutputStream(file);
            ObjectOutputStream OOS = new ObjectOutputStream(fos);
            OOS.writeObject(friendList);
            OOS.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WriteFriendList.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
