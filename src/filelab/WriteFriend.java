/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filelab;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class WriteFriend {

    public static void main(String[] args) {
        FileOutputStream fos = null;
        try {
            Friend friend1 = new Friend("Karntima ", 21, "0884561237");
            Friend friend2 = new Friend("Karntima ", 23, "0944561237");
            Friend friend3 = new Friend("Karntima ", 25, "0948745637");
            //friend.dat
            File file = new File("friends.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream OOS = new ObjectOutputStream(fos);
            OOS.writeObject(friend1);
            OOS.writeObject(friend2);
            OOS.writeObject(friend3);
            OOS.close( );
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }
}
